# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  res = {}
  str.split.each do |word|
    res[word] = word.length
  end
  res
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v }.last.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letters = Hash.new(0)
  word.chars do |ch|
    letters[ch] += 1
  end
  letters
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  some_hash = {}
  arr.each do |el|
    some_hash[el] = "meow"
  end
  some_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  res_hash = {:even => 0, :odd => 0}
  numbers.each do |num|
    if num.even?
      res_hash[:even] += 1
    else
      res_hash[:odd] += 1
    end
  end
  res_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = "aeiou"
  vowel_count = Hash.new(0)
  string.chars.each do |ch|
    if vowels.include?(ch)
      vowel_count[ch] += 1
    end
  end
  vowel_count.sort_by { |k, v| v }.last.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  fall_winter_students = students.select { |k, v| v > 6 }.keys
  res = []
  fall_winter_students.each_index do |idx|
    ((idx + 1)...fall_winter_students.length).each do |jdx|
      res << [fall_winter_students[idx], fall_winter_students[jdx]]
    end
  end
  res
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  spec_hash = Hash.new(0)
  specimens.each do |specimen|
    spec_hash[specimen] += 1
  end
  number_of_species = spec_hash.length
  largest_population_size = spec_hash.values.max
  smallest_population_size = spec_hash.values.min
  number_of_species ** 2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign.delete!(".,?/!`~';'` []{_+(")
  vandalized_sign.delete!(".,?/!`~';'` []{_+(c)")
  norm_letters = character_count(normal_sign.downcase)
  vand_letters = character_count(vandalized_sign.downcase)
  vand_letters.each do |k, v|
    return false if norm_letters[k] < v
  end
  true
end

def character_count(str)
  res_hash = Hash.new(0)
  str.each_char do |ch|
    res_hash[ch] += 1
  end
  res_hash
end
